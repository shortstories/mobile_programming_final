package com.navercorp.finalterm.helper;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;

import java.lang.reflect.Field;

/**
 * Created by kwons on 2015-10-29.
 */
public class BitmapHelper {
    public static Bitmap createCroppedBitmap(int containerHeight, int containerWidth, Bitmap img) {
        int imgHeight = img.getHeight();
        int imgWidth = img.getWidth();
        //boolean ch2cw = containerHeight > containerWidth;
        //float h2w = (float) imgHeight / (float) imgWidth;
        float newImageHeight, newImageWidth;

/*        if (h2w > 1) {
            // height is greater than width
            if (ch2cw) {
                newContainerWidth = (float) containerWidth;
                newContainerHeight = newContainerWidth * h2w;
            } else {
                newContainerHeight = (float) containerHeight;
                newContainerWidth = newContainerHeight / h2w;
            }
        } else {
            // width is greater than height
            if (ch2cw) {
                newContainerWidth = (float) containerWidth;
                newContainerHeight = newContainerWidth / h2w;
            } else {
                newContainerWidth = (float) containerHeight;
                newContainerHeight = newContainerWidth * h2w;
            }
        }*/

        newImageHeight = containerHeight;
        newImageWidth = imgWidth + containerHeight - imgHeight;
        Bitmap resizeBitmap = Bitmap.createScaledBitmap(img, (int) newImageWidth,
                (int) newImageHeight, false);

        Bitmap croppedBitmap = Bitmap.createBitmap(resizeBitmap, 0, 0, containerWidth, (int)newImageHeight);

        return croppedBitmap;
    }
    public static Bitmap createCroppedBitmap(View v, Bitmap img) {
        return createCroppedBitmap(v.getHeight(), v.getWidth(), img);
    }
    public static Bitmap createCroppedBitmap(Display display, Bitmap img) {
        DisplayMetrics metrics = new DisplayMetrics();
        display.getMetrics(metrics);

        int height = metrics.heightPixels;
        int width = metrics.widthPixels;

        return createCroppedBitmap(height, width, img);
    }

    public static Bitmap drawableToBitmap (Drawable drawable) {
        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable)drawable).getBitmap();
        }

        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(),
                drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    public static int getResId(String resName, Class<?> c) {
        try {
            Field idField = c.getDeclaredField(resName);
            return idField.getInt(idField);
        }
        catch(Exception e) {
            e.printStackTrace();
            return -1;
        }
    }
}
