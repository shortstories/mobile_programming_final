package com.navercorp.finalterm;

import android.Manifest;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.navercorp.finalterm.NMap.NMapPOIflagType;
import com.navercorp.finalterm.NMap.NMapViewerResourceProvider;
import com.navercorp.finalterm.model.Monster;
import com.navercorp.finalterm.model.MonsterCaught;
import com.navercorp.finalterm.model.MonsterWild;
import com.nhn.android.maps.NMapActivity;
import com.nhn.android.maps.NMapController;
import com.nhn.android.maps.NMapLocationManager;
import com.nhn.android.maps.NMapView;
import com.nhn.android.maps.maplib.NGeoPoint;
import com.nhn.android.maps.nmapmodel.NMapError;
import com.nhn.android.maps.overlay.NMapPOIdata;
import com.nhn.android.mapviewer.overlay.NMapMyLocationOverlay;
import com.nhn.android.mapviewer.overlay.NMapOverlayManager;
import com.nhn.android.mapviewer.overlay.NMapPOIdataOverlay;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.LinkedList;
import java.util.List;

public class MainActivity extends NMapActivity {
    final static String PROXIMITY = "com.navercorp.finalterm.MainActivity.Proximity";
    final float proximity_radius = 30f;
    final static int ENCOUNTER = 1;

    LocationManager mLocationManager;
    LocationListener mLocationListener;
    Location currentLocation;

    NMapView nMapView;
    NMapController nMapController;
    NMapLocationManager nMapLocationManager;
    NMapOverlayManager nMapOverlayManager;
    NMapMyLocationOverlay nMapMyLocationOverlay;
    NMapViewerResourceProvider nMapViewerResourceProvider;

    ProximityIntentReceiver proximityIntentReceiver;

    List<PendingIntent> alertList;
    List<String> checkList;

    boolean flag_proximity_register = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        mLocationListener = new MyLocationListener();

        // 권한 체크
        if ((ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) && (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) ) {
            Toast.makeText(this, "위치 정보 관련 권한이 부족합니다.", Toast.LENGTH_LONG).show();
            finish();
        }

        // NETWORK PROVIDER
        if (mLocationManager.getAllProviders().contains(LocationManager.NETWORK_PROVIDER)) {
            mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
                    0, 0, mLocationListener);
            currentLocation = mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        }

        // GPS PROVIDER
        if (mLocationManager.getAllProviders().contains(LocationManager.GPS_PROVIDER)) {
            mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                    0, 0, mLocationListener);
            currentLocation = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        }

        // naver map initialize
        //nMapView = new NMapView(this);
        nMapView = (NMapView)findViewById(R.id.map);
        nMapView.setApiKey(getString(R.string.naver_map_key));

        //setContentView(nMapView);

        // naver map settings
        nMapView.setClickable(true);
        MyMapStateChangeHandler handler = new MyMapStateChangeHandler();
        //nMapView.setOnMapViewTouchEventListener();
        nMapView.setOnMapStateChangeListener(handler);


        nMapController = nMapView.getMapController();

        nMapLocationManager = new NMapLocationManager(getApplicationContext());
//        nMapLocationManager.setOnLocationChangeListener(new NMapLocationManager.OnLocationChangeListener() {
//            @Override
//            public boolean onLocationChanged(NMapLocationManager nMapLocationManager, NGeoPoint nGeoPoint) {
//                Log.i(getClass().getSimpleName(), "onLocationChanged");
//
//                nMapController.animateTo(nGeoPoint);
//
//                if (!nMapOverlayManager.hasOverlay(nMapMyLocationOverlay)) {
//                    nMapOverlayManager.addOverlay(nMapMyLocationOverlay);
//                }
//
//                nMapView.postInvalidate();
//                return false;
//            }
//
//            @Override
//            public void onLocationUpdateTimeout(NMapLocationManager nMapLocationManager) {
//
//            }
//
//            @Override
//            public void onLocationUnavailableArea(NMapLocationManager nMapLocationManager, NGeoPoint nGeoPoint) {
//
//            }
//        });


        nMapViewerResourceProvider = new NMapViewerResourceProvider(getApplicationContext());

        nMapOverlayManager = new NMapOverlayManager(getApplicationContext(),
                nMapView, nMapViewerResourceProvider);

        nMapMyLocationOverlay = nMapOverlayManager.createMyLocationOverlay(nMapLocationManager, null);

        alertList = new LinkedList<>();
        proximityIntentReceiver = new ProximityIntentReceiver();

        findViewById(R.id.btn_pokedex).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, DexActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

        findViewById(R.id.btn_current_position).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentLocation != null) {
                    double x = currentLocation.getLatitude(), y = currentLocation.getLongitude();
                    nMapController.animateTo(new NGeoPoint(y, x));
                }
            }
        });

        checkList = new LinkedList<>();
    }

    @Override
    protected void onResume() {
        super.onResume();

        nMapOverlayManager.clearOverlays();
        nMapLocationManager.enableMyLocation(false);

        ParseQuery<MonsterCaught> caughtQuery = ParseQuery.getQuery(MonsterCaught.class);
        caughtQuery.whereEqualTo("user", ParseUser.getCurrentUser());
        try {
            List<MonsterCaught> tempList = caughtQuery.find();
            for (MonsterCaught mon : tempList) {
                checkList.add(mon.getMonsterWild().getObjectId());
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        ParseQuery<MonsterWild> query = ParseQuery.getQuery(MonsterWild.class);
        query.findInBackground(new FindCallback<MonsterWild>() {
            @Override
            public void done(List<MonsterWild> list, ParseException e) {
                if (e == null) {
                    int markerId = NMapPOIflagType.PIN;
                    int i = 0;

                    NMapPOIdata poiData = new NMapPOIdata(list.size(), nMapViewerResourceProvider);
                    poiData.beginPOIdata(list.size());
                    for (MonsterWild monster : list) {
                        if (checkList.contains(monster.getObjectId())) {
                            continue;
                        }

                        ParseGeoPoint geo = monster.getLocation();
                        poiData.addPOIitem(geo.getLongitude(), geo.getLatitude(), null, markerId, 0);

                        // register filter
                        String intentAction = PROXIMITY + i;
                        IntentFilter filter = new IntentFilter(intentAction);
                        registerReceiver(proximityIntentReceiver, filter);
                        flag_proximity_register = true;

                        // get PendingIntent
                        Intent intent = new Intent(intentAction);
                        intent.putExtra(MonsterWild.MONSTER_WILD_ID, monster.getObjectId());
                        PendingIntent proximityIntent = PendingIntent.getBroadcast(MainActivity.this,
                                i, intent, PendingIntent.FLAG_CANCEL_CURRENT);

                        // set Proximity Alert
                        mLocationManager.addProximityAlert(geo.getLatitude(),
                                geo.getLongitude(), proximity_radius, -1, proximityIntent);

                        // add PendingIntent List
                        alertList.add(proximityIntent);

                        i++;
                    }
                    poiData.endPOIdata();
                    NMapPOIdataOverlay poiDataOverlay
                            = nMapOverlayManager.createPOIdataOverlay(poiData, null);
                }
                else {
                    Log.d(getClass().getSimpleName(), e.getMessage());
                }
            }
        });


    }

    @Override
    protected void onPause() {
        super.onPause();
        nMapLocationManager.disableMyLocation();

        if (flag_proximity_register) unregisterReceiver(proximityIntentReceiver);
        flag_proximity_register = false;

        for (PendingIntent intent : alertList) {
            mLocationManager.removeProximityAlert(intent);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }

    private class MyLocationListener implements LocationListener {
        @Override
        public void onLocationChanged(Location location) {
            currentLocation = location;
            double x = location.getLatitude(), y = location.getLongitude();

            nMapController.animateTo(new NGeoPoint(y, x));
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    }

    private class MyMapStateChangeHandler implements NMapView.OnMapStateChangeListener {
        @Override
        public void onMapInitHandler(NMapView nMapView, NMapError nMapError) {
            if (currentLocation != null) {
                if (nMapError == null) {
                    nMapController.setMapCenter(currentLocation.getLongitude(), currentLocation.getLatitude(), 20);
                } else {
                    Log.e(getClass().getSimpleName(), nMapError.toString());
                }
            }
        }
        @Override
        public void onMapCenterChange(NMapView nMapView, NGeoPoint nGeoPoint) {

        }

        @Override
        public void onMapCenterChangeFine(NMapView nMapView) {

        }

        @Override
        public void onZoomLevelChange(NMapView nMapView, int i) {

        }

        @Override
        public void onAnimationStateChange(NMapView nMapView, int i, int i1) {

        }
    }
    public class ProximityIntentReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String id = intent.getStringExtra(MonsterWild.MONSTER_WILD_ID);
            checkList.add(id);
            // Toast.makeText(context, id, Toast.LENGTH_LONG).show();

            Intent encounterIntent = new Intent(MainActivity.this, EncounterActivity.class);
            encounterIntent.putExtra(MonsterWild.MONSTER_WILD_ID, id);
            encounterIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivityForResult(encounterIntent, ENCOUNTER);
        }
    }
}