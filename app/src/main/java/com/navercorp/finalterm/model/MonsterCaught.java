package com.navercorp.finalterm.model;

import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseUser;

/**
 * Created by Administrator on 2015-12-16.
 */
@ParseClassName("monster_caught")
public class MonsterCaught extends ParseObject {
    public Monster getMonster() {
        return (Monster)get("monster");
    }
    public void setMonster(Monster monster) {
        put("monster", monster);
    }

    public MonsterWild getMonsterWild() {
        return (MonsterWild)get("monster_wild");
    }
    public void setMonsterWild(MonsterWild monsterWild) {
        put("monster_wild", monsterWild);
    }


    public ParseUser getUser() {
        return (ParseUser)get("user");
    }
    public void setUser(ParseUser user) {
        put("user", user);
    }
}
