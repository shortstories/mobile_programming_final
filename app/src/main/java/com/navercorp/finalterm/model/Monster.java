package com.navercorp.finalterm.model;

import com.parse.ParseClassName;
import com.parse.ParseObject;

/**
 * Created by Administrator on 2015-12-16.
 */

@ParseClassName("monster")
public class Monster extends ParseObject {
    public int getNo() {
        return getInt("no");
    }
    public void setNo(int val) {
        put("no", val);
    }

    public String getName() {
        return getString("name");
    }
    public void setName(String val) {
        put("name", val);
    }
}
