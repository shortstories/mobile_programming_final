package com.navercorp.finalterm.model;

import com.parse.ParseClassName;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;

/**
 * Created by Administrator on 2015-12-16.
 * 야생 포켓몬스터
 */
@ParseClassName("monster_wild")
public class MonsterWild extends ParseObject {
    public final static String MONSTER_WILD_ID = "com.navercorp.finalterm.MainActivity.MONSTER_ID";

    public Monster getMonster() {
        return (Monster)get("monster");
    }
    public void setMonster(Monster monster) {
        put("monster", monster);
    }

    public ParseGeoPoint getLocation() {
        return (ParseGeoPoint)get("location");
    }
    public void setLocation(ParseGeoPoint location) {
        put("location", location);
    }

    public String getDescription() {
        return (String)get("description");
    }
    public void setDescription(String description) {
        put("description", description);
    }
}
