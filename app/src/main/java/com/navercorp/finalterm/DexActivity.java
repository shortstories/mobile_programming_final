package com.navercorp.finalterm;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.navercorp.finalterm.helper.BitmapHelper;
import com.navercorp.finalterm.model.Monster;
import com.navercorp.finalterm.model.MonsterCaught;
import com.navercorp.finalterm.model.MonsterWild;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.List;

public class DexActivity extends Activity {

    ListView listPokemon;
    List<Monster> pokemonList;
    List<MonsterCaught> pokemonCaughtList;

    PokemonAdapter pokemonAdapter;
    View mProgressView;

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            listPokemon.setVisibility(show ? View.GONE : View.VISIBLE);
            listPokemon.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    listPokemon.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            listPokemon.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    public Dialog getMonsterInfoDialog(Monster monster, MonsterWild monsterWild) {
        View dialogView = LayoutInflater.from(this).inflate(R.layout.dialog_monster_info, null);

        String nextResId = "mon_" + monster.getNo();
        ((ImageView)dialogView.findViewById(R.id.img_monster))
                .setImageDrawable(
                        getResources().getDrawable(
                                BitmapHelper.getResId(nextResId, R.drawable.class),
                                getTheme()
                        )
                );
        ((TextView)dialogView.findViewById(R.id.label_monster_name)).setText(monster.getName());
        ((TextView)dialogView.findViewById(R.id.label_description)).setText(monsterWild.getDescription());

        return new AlertDialog.Builder(this)
                .setView(dialogView)
                .create();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dex);
        listPokemon = (ListView)findViewById(R.id.list_pokemon);
        mProgressView = findViewById(R.id.loading_progress);

        ParseQuery<Monster> pokemonQuery = ParseQuery.getQuery(Monster.class);
        pokemonQuery.setLimit(151);
        pokemonQuery.orderByAscending("no");

        ParseQuery<MonsterCaught> caughtQuery = ParseQuery.getQuery(MonsterCaught.class);
        caughtQuery.setLimit(300);
        caughtQuery.whereEqualTo("user", ParseUser.getCurrentUser());

        showProgress(true);
        pokemonQuery.findInBackground(new FindCallback<Monster>() {
            @Override
            public void done(List<Monster> list, ParseException e) {
                pokemonList = list;

                if (pokemonList != null && pokemonCaughtList != null) {
                    pokemonAdapter = new PokemonAdapter(DexActivity.this, pokemonList, pokemonCaughtList);
                    listPokemon.setAdapter(pokemonAdapter);
                }

                showProgress(false);
            }
        });
        caughtQuery.findInBackground(new FindCallback<MonsterCaught>() {
            @Override
            public void done(List<MonsterCaught> list, ParseException e) {
                pokemonCaughtList = list;

                try {
                    MonsterCaught.unpinAll();
                    MonsterCaught.pinAll(pokemonCaughtList);
                } catch (ParseException e1) {
                    e1.printStackTrace();
                }

                if (pokemonList != null && pokemonCaughtList != null) {
                    pokemonAdapter = new PokemonAdapter(DexActivity.this, pokemonList, pokemonCaughtList);
                    listPokemon.setAdapter(pokemonAdapter);
                }

                showProgress(false);
            }
        });

        listPokemon.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (view.findViewById(R.id.img_ball).getVisibility() != View.GONE) {
                    ParseQuery<MonsterCaught> caughtQuery = ParseQuery.getQuery(MonsterCaught.class);
                    caughtQuery.fromLocalDatastore();
                    caughtQuery.whereEqualTo("monster", pokemonAdapter.getItem(position));
                    List<MonsterCaught> list;
                    try {
                        list = caughtQuery.find();
                        if (list.size() > 0) {
                            MonsterCaught monsterCaught = list.get(0);
                            MonsterWild monsterWild = monsterCaught.getMonsterWild().fetchIfNeeded();
                            Monster monster = monsterCaught.getMonster().fetchIfNeeded();

                            getMonsterInfoDialog(monster, monsterWild).show();
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }


    private class PokemonAdapter extends BaseAdapter {
        Context mContext;
        List<Monster> pokemonList;
        List<MonsterCaught> pokemonCaughtList;

        public PokemonAdapter(Context context, List<Monster> pokemonList,
                              List<MonsterCaught> pokemonCaughtList) {
            this.mContext = context;
            this.pokemonList = pokemonList;
            this.pokemonCaughtList = pokemonCaughtList;
        }

        @Override
        public int getCount() {
            return pokemonList.size();
        }

        @Override
        public Object getItem(int position) {
            return pokemonList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View item;
            Monster pokemon = (Monster)getItem(position);
            String thumbResId = "mon_thumb_" + pokemon.getNo();

            if (convertView != null) {
                item = convertView;
            }
            else {
                item = LayoutInflater.from(mContext).inflate(R.layout.dex_item, parent, false);
            }

            ((TextView)item.findViewById(R.id.label_name)).setText(pokemon.getName());
            ((TextView)item.findViewById(R.id.label_no)).setText(String.valueOf(pokemon.getNo()));
            ((ImageView)item.findViewById(R.id.img_thumb))
                    .setImageDrawable(getDrawable(BitmapHelper.getResId(thumbResId, R.drawable.class)));

            ParseQuery<MonsterCaught> caughtQuery = ParseQuery.getQuery(MonsterCaught.class);
            caughtQuery.fromLocalDatastore();
            caughtQuery.whereEqualTo("monster", pokemon);
            List<MonsterCaught> list = null;
            try {
                list = caughtQuery.find();
            } catch (ParseException e) {
                e.printStackTrace();
            }

            if (list != null && list.size() > 0) {
                item.findViewById(R.id.img_ball).setVisibility(View.VISIBLE);
                item.setEnabled(true);
            }
            else {
                item.findViewById(R.id.img_ball).setVisibility(View.GONE);
                item.setEnabled(false);
            }

            return item;
        }
    }
}
