package com.navercorp.finalterm;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.navercorp.finalterm.helper.BitmapHelper;
import com.navercorp.finalterm.model.Monster;
import com.navercorp.finalterm.model.MonsterCaught;
import com.navercorp.finalterm.model.MonsterWild;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;

public class EncounterActivity extends Activity {

    MonsterWild encounterMonsterWild;
    Monster encounterMonster;

    TextView labelMessage;
    TextView labelMonsterName;
    LinearLayout layoutControlButtons;

    ImageView imgMonster;

    SensorManager mSensorManager;
    CatchDetector catchDetector;


    public void catchMonster(boolean isCaught) {
        if (isCaught) {
            MonsterCaught monsterCaught = new MonsterCaught();
            monsterCaught.setMonster(encounterMonster);
            monsterCaught.setMonsterWild(encounterMonsterWild);
            monsterCaught.setUser(ParseUser.getCurrentUser());

            try {
                monsterCaught.save();
                Toast.makeText(EncounterActivity.this,
                        encounterMonster.getName() + "을(를) 잡았다!", Toast.LENGTH_SHORT).show();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        else {
            Toast.makeText(EncounterActivity.this, "포켓몬이 도망갔습니다.", Toast.LENGTH_LONG)
                    .show();
        }

        mSensorManager.unregisterListener(catchDetector);
        finish();
    }

    public void escape() {
        Toast.makeText(EncounterActivity.this, "성공적으로 도망쳤습니다.", Toast.LENGTH_LONG)
                .show();
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_encounter);

        labelMessage = (TextView)findViewById(R.id.label_message);
        labelMonsterName = (TextView)findViewById(R.id.label_monster_name);
        layoutControlButtons = (LinearLayout)findViewById(R.id.layout_control_buttons);

        imgMonster = (ImageView)findViewById(R.id.img_monster);

        String monsterId = getIntent().getStringExtra(MonsterWild.MONSTER_WILD_ID);

        ParseQuery<MonsterWild> query = ParseQuery.getQuery(MonsterWild.class);
        try {
            encounterMonsterWild = query.get(monsterId);
            encounterMonster = encounterMonsterWild.getMonster().fetchIfNeeded();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (encounterMonster != null) {
            String nextResId = "mon_" + encounterMonster.getNo();
            String message = encounterMonster.getName() + "을(를) 만났다!\n"
                    + "(잡으면 도감에서 확인 가능)";
            labelMonsterName.setText(encounterMonster.getName());
            labelMessage.setText(message);

            imgMonster.setImageDrawable(
                    getResources().getDrawable(BitmapHelper.getResId(nextResId, R.drawable.class))
            );

            labelMessage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    v.setVisibility(View.GONE);
                    layoutControlButtons.setVisibility(View.VISIBLE);
                }
            });

            layoutControlButtons.findViewById(R.id.btn_catch).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String message = "휴대폰을 위에서 아래로 휘두르세요\n"
                            + "힘이 부족하면 도망가게 됩니다.";
                    labelMessage.setText(message);
                    labelMessage.setVisibility(View.VISIBLE);
                    layoutControlButtons.setVisibility(View.GONE);

                    mSensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
                    catchDetector = new CatchDetector(EncounterActivity.this, 15);
                    mSensorManager.registerListener(catchDetector,
                            mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                            SensorManager.SENSOR_DELAY_UI);
                }
            });
            layoutControlButtons.findViewById(R.id.btn_escape).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    escape();
                }
            });
        }
    }

    @Override
    public void onBackPressed() {
        escape();
    }

    private class CatchDetector implements SensorEventListener {
        Context mContext;
        float threshold;
        float currentY;
        float previousY;

        public CatchDetector(Context context, float threshold) {
            this.mContext = context;
            this.threshold = threshold;

            currentY = previousY = 0;
        }

        @Override
        public void onSensorChanged(SensorEvent event) {
            float x, y, z;
            x = event.values[0];
            y = event.values[1];
            z = event.values[2];

            currentY = y;

            //Log.i(getClass().getSimpleName(), "cY: " + currentY + ", pY: " + previousY );
            //Log.i(getClass().getSimpleName(), Math.abs(currentY - previousY) + "");
            if (Math.abs(currentY - previousY) > threshold) {
                catchMonster(true);
            }
            else if (Math.abs(currentY - previousY) > (threshold - 5)) {
                catchMonster(false);
            }

            previousY = y;
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {

        }
    }
}
