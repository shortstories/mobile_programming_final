package com.navercorp.finalterm;

import android.app.Application;

import com.facebook.FacebookSdk;
import com.navercorp.finalterm.model.Monster;
import com.navercorp.finalterm.model.MonsterCaught;
import com.navercorp.finalterm.model.MonsterWild;
import com.parse.Parse;
import com.parse.ParseFacebookUtils;
import com.parse.ParseObject;

/**
 * Created by kwons on 2015-12-16.
 */
public class BaseApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        FacebookSdk.sdkInitialize(getApplicationContext());

        Parse.enableLocalDatastore(getApplicationContext());

        ParseObject.registerSubclass(Monster.class);
        ParseObject.registerSubclass(MonsterCaught.class);
        ParseObject.registerSubclass(MonsterWild.class);

        Parse.initialize(getApplicationContext());

        ParseFacebookUtils.initialize(getApplicationContext());
    }
}
